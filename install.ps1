Set-Location -Path .\

Write-Host -ForegroundColor DarkGreen "********************************"
Write-Host -ForegroundColor DarkGreen "********** Setup ***********"
Write-Host -ForegroundColor DarkGreen "********************************"

# Vars
$context = "$(Get-Location)"
$dockerPath = "$context\docker"
$dockerComposePath = "$context\docker\cli-plugins"
$zipArchive = "ubuntu.zip.001"
$dockerHost = "tcp://127.0.0.1:2375"
$7zPath = "$env:ProgramFiles\7-Zip"

# Is 7zip installed?
if (-not (Test-Path $7zPath)) {
    if (-not (winget install 7zip -s winget)) {
        throw "Choooezhed up? where the �hoooezh is 7z? Choooezhed up winget?"
    }
}

$env:PATH += ";$7zPath"

# Expand distro
7z e $zipArchive

Write-Host -ForegroundColor DarkGreen "********************************"

# Install wsl
#wsl --install

# Import distro
if (wsl --import ubuntu-docker $env:HOME\WSL .\ubuntu) {
    Write-Host "Imported Distro"
}

Write-Host -ForegroundColor DarkGreen "********************************"

# Set environment
$env:Path += ";$dockerPath"
Write-Host "Added PATH $($dockerPath)"

$env:Path += ";$dockerComposePath"
Write-Host "Added PATH $dockerComposePath"

[environment]::SetenvironmentVariable("Path", $env:Path, [System.environmentVariableTarget]::User)
Write-Output "Setted PATH"

Write-Host -ForegroundColor DarkGreen "********************************"

$env:DOCKER_HOST = "$dockerHost"
[environment]::SetenvironmentVariable("DOCKER_HOST", $env:DOCKER_HOST, [System.environmentVariableTarget]::User)
Write-Host "Added DOCKER_HOST $env:DOCKER_HOST"

Write-Host -ForegroundColor DarkGreen "********************************"

# Start linux
# wsl -d ubuntu-docker